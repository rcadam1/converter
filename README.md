# Converter

Proyecto de 1º de DAM que consiste en un conversor de decimal a binario mediante un archivo de texto.

### Versión 1.0

Primera versión del proyecto monolítica y en línea de comandos. Recibe como parámetro un archivo con números entero
(uno por línea) para convertirlos en binario.

### Versión 2.0

Segunda versión del proyecto modular y en línea de comandos. Recibe como parámetro un archivo con números enteros (uno
por línea) para convertirlos en binario.

### Versión 3.0

Versión GUI del proyecto de conversión de números decimales a binario mediante ficheros de texto.

### Versión 4.0

Versión GUI del proyecto con barra de menús.

### Versión 4.1

Mejora con la visibilidad de la acción 'Convert'.

---

## Pasos de la creación del proyecto

Esta aplicación ha sido escrita en Visual Studio Code que genera un proyecto de Eclipse e incluye soporte para Maven, aunque todos los comandos se han ejecutado desde la consola. También se ha usado Git para el control de versiones y la sincronización con GitLab.

#### Maven

Se ha inicializado el proyecto con:

        $ mvn archetype:generate \
                -DgroupId=org.dam.converter \
                -DartifactId=Converter \
                -Dversion=1.0 \
                -DarchtypeArtifactId=maven-archetype-quickstart \
                -DarchetypeVersion=1.4

A continuación se ha cambiado la versión del compilador de java en el archivo pom.xml de la 1.7 a la 1.8. En cada versión que se ha ido creando también se ha cambiado el número de versión del proyecto en el archivo pom.xml.

Cada versión se ha compilado con:

        $ mvn compile

Y se ha creado el jar ejecutable añadiendo las líneas:

          <configuration>
            <archive>
              <manifest>
                <mainClass>org.dam1.converter.Main</mainClass>
              </manifest>
            </archive>
          </configuration>

al plugin 'maven-jar-plugin' en el pom.xml y se ha ejecutado el siguiente comando:

        $ mvn package

Todas las versiones de los archivos jar se han ido moviendo al directorio raíz del proyecto porque el directorio target no se incluye en la sincronización con git.

#### Git

Se ha inicializacido el repositorio con:

        $ git init

y se ha creado el archivo .gitignore con los archivos y directorios que no necesitan seguimiento. También se ha creado el archivo README.md con las notas de las versiones y los pasos para la creación del proyecto.

Después se ha ejecutado un commit inicial con:

        $ git add .
        $ git commit

También se ha creado un repositorio remoto en GitLab para la sincronización en la nube y se ha añadido el remoto con:

        $ git remote add origin https://rudimerlos@gitlab.com/rudimerlos/converter.git

y se ha sincronizado la rama master con:

        $ git push origin master

Las dos primeras versiones del proyecto se han hecho en la rama master pero a partir de la tercera se ha creado otra rama llamada gui con:

        $ git checkout -b gui

que crea la rama gui y cambia a ésta para crear la tercera versión del proyecto.

Una vez creada esta tercera versión, se ha commiteado en gui y, a continuación se ha fucionado con master:

        $ git checkout master
        $ git merge gui

El resto de versiones se han realizado en la rama master.
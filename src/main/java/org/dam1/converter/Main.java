package org.dam1.converter;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * Clase principal de la aplicación Converter.
 * @param args Argumentos recibidos.
 */
public class Main {


    public static void main(String[] args) {
        FrameConverter frame = new FrameConverter();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}


/**
 * Conversor de binario a decimal mediante un fichero de texto. La aplicación lee un fichero de
 * texto con valores numéricos (uno por línea) y muestra su valor en binario.
 */
class FrameConverter extends JFrame {

    private static final long serialVersionUID = 1L;

    private final String info =
            "Converter v4.0\nConversor de números decimales enteros a binario.\n\npor Rudi Merlos Carrión\nAbril 2020";

    private JMenuItem miOpen;
    private JMenuItem miSave;
    private JMenuItem miExit;
    private JMenuItem miCopy;
    private JMenuItem miConvert;
    private JMenuItem miAbout;
    private JPanel buttons;
    private JButton btnConvert;
    private JTextArea area;
    private JScrollPane scroll;

    private File file;
    private Vector<String> vector;

    private boolean converted;

    private final JFileChooser fc = new JFileChooser();

    public FrameConverter() {
        super("Binary Converter");
        setSize(400, 600);
        setResizable(false);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        // Inicialización de varibles para la gestión de la aplicación
        file = null;
        vector = null;
        converted = false;

        // Barra de menú
        miOpen = new JMenuItem("Open");
        miSave = new JMenuItem("Save");
        miSave.setEnabled(false);
        miExit = new JMenuItem("Exit");

        miCopy = new JMenuItem("Copy");
        miConvert = new JMenuItem("Convert");
        miConvert.setEnabled(false);

        miAbout = new JMenuItem("About");

        JMenu mFile = new JMenu("File");
        mFile.add(miOpen);
        mFile.add(miSave);
        mFile.addSeparator();
        mFile.add(miExit);
        JMenu mEdit = new JMenu("Edit");
        mEdit.add(miCopy);
        mEdit.addSeparator();
        mEdit.add(miConvert);
        JMenu mHelp = new JMenu("Help");
        mHelp.add(miAbout);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(mFile);
        menuBar.add(mEdit);
        menuBar.add(mHelp);
        setJMenuBar(menuBar);

        // Botónes
        buttons = new JPanel();
        btnConvert = new JButton("Convert");
        btnConvert.setEnabled(false);
        buttons.add(btnConvert);

        // Área de texto
        area = new JTextArea();
        area.setEditable(false);
        scroll = new JScrollPane(area);

        // Añadir los elementos al frame
        add(scroll, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);

        // Eventos
        ActionManager am = new ActionManager();
        miOpen.addActionListener(am);
        miSave.addActionListener(am);
        miExit.addActionListener(am);
        miCopy.addActionListener(am);
        miConvert.addActionListener(am);
        miAbout.addActionListener(am);
        btnConvert.addActionListener(am);
    }

    /**
     * Intenta parsear una cadena de texto a tipo long y, si lo consigue lo guarda en otra cadena
     * en base 2.
     *
     * @param line Cadena de entrada
     * @return Cadena con el número en binario o cadena vacía si no lo puede parsear
     */
    private String getBinary(String line) {
        try {
            // Intenta convertir la línea a long
            long n = Long.parseLong(line);
            String bin = ""; // Cadena donde guardar el entero en binario al revés
            while (n > 1) {
                bin += (n % 2);
                n /= 2;
            }
            bin += n;
            // Invierte el orden del string para mostrar el número en binario
            bin = new StringBuilder(bin).reverse().toString();
            return bin;
        } catch (NumberFormatException e) {
            return "";
        }
    }

    /**
     * Limpia el área de texto y la actualiza.
     */
    private void updateArea() {
        if (vector != null) {
            area.setText(null);
            for (String str : vector) {
                area.append(str + "\n");
            }
        }
    }

    /**
     * Permite abrir un archivo mediante un menú de selección, rellena el vector con las líneas y
     * actualiza el área de texto.
     */
    private void openFile() {
        int value = fc.showOpenDialog(null);
        if (value == JFileChooser.APPROVE_OPTION) {
            file = fc.getSelectedFile();
            vector = new Vector<String>();
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String line = reader.readLine();
                while (line != null) {
                    vector.add(line);
                    line = reader.readLine();
                }
                reader.close();
                converted = false;
                updateArea();
                miSave.setEnabled(false);
                miConvert.setEnabled(true);
                btnConvert.setEnabled(true);
            } catch (FileNotFoundException e) {
                JOptionPane.showMessageDialog(this, "No se ha encontrado el archivo",
                        "Error al abrir", JOptionPane.ERROR_MESSAGE);
                System.err.println("No se ha encontrado el archivo");
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "Error en la lectura del archivo",
                        "Error al abrir", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Convierte los valores del vector a binario y actualiza el área de texto.
     */
    private void convert() {
        if (vector != null && !converted) {
            int index = 0;
            for (String str : vector) {
                String bin = getBinary(str);
                if (!bin.isEmpty()) {
                    vector.setElementAt(bin, index);
                    index++;
                } else {
                    vector.removeElementAt(index);
                }
            }
            converted = true;
            updateArea();
            miSave.setEnabled(true);
            miConvert.setEnabled(false);
            btnConvert.setEnabled(false);
        }
    }

    /**
     * Permite guardar un archivo mediante un menú de selección.
     * Si guarda al archivo exitosamente borra el vector.
     */
    private void saveFile() {
        int value = fc.showSaveDialog(null);
        if (value == JFileChooser.APPROVE_OPTION) {
            file = fc.getSelectedFile();
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                if (vector != null) {
                    for (String str : vector) {
                        writer.write(str);
                        writer.newLine();
                    }
                }
                writer.close();
                miSave.setEnabled(false);
                vector.clear();
                vector = null;
                JOptionPane.showMessageDialog(this, "Archivo guardado con éxito",
                        "Archivo guardado", JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "Error en la escritura del archivo",
                        "Error al guardar", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Clase interna para gestionar los eventos.
     */
    private class ActionManager implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == miOpen) {
                openFile();
            } else if (e.getSource() == miSave) {
                saveFile();
            } else if (e.getSource() == miCopy) {
                area.copy();
            } else if (e.getSource() == btnConvert || e.getSource() == miConvert) {
                convert();
            } else if (e.getSource() == miAbout) {
                JOptionPane.showMessageDialog(null, info, "About Converter",
                        JOptionPane.INFORMATION_MESSAGE);
            } else {
                System.exit(0);
            }
        }
    }
}